\documentclass[french,11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{comment}
\usepackage{tikz}
\usepackage{tikzsymbols}
\usepackage{tabularx}
\usepackage{tkz-tab}
\usepackage[babel=true,kerning=true]{microtype}
\usepackage{enumerate}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{fancyvrb}
\usepackage{lastpage} %% Pour obtenir le nombre de page
\usepackage[left=1.8cm,right=1.8cm,top=1.4cm,bottom=1.2cm]{geometry}
\usepackage[algoruled,vlined]{algorithm2e}

\begin{document}

\pagestyle{fancy}
\lhead{\textit{Projet: Simulateur de machine de Turing}}
\chead{}
\rhead{LSIN 608 UVSQ}
\lfoot{}
\cfoot{Page \thepage / \pageref{LastPage}}
\rfoot{}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}


\leavevmode
\vfill

\begin{center}
\shadowbox{\begin{minipage}{13cm}
\begin{center}
  \bigskip
\Huge{{\fontfamily{ptm}\selectfont{ Projet: \\ Simulateur de machine de Turing \\ Cahier des charges}}}

  \bigskip
\end{center}
\end{minipage}}
\end{center}

\vfill
\Large{ \textbf{Auteurs:}}

\begin{itemize}
  \item Helmi Abed
  \item Tahar Azouaoui
  \item Louis Leskow
  \item Mohamed Louiba
  \item Farah Mahrez
  \item Ahmed Saadi
  \item Wissem Sassi
  \item Martin Sohier
\end{itemize}

\vfill

\newpage

\normalsize
\section{Description générale}

\subsection{Présentation}

Une machine de Turing est un modèle théorique des \og{}machines de calculs\fg{}
permettant de réaliser des procédures algorithmiques.
On imagine avoir un ou plusieurs rubans infinis, ceux-ci sont constitutés
d'une infinité de cases pouvant contenir un symbole. Chaque ruban est également
munit d'une tête de lecture pouvant lire le symbole contenu dans une case et y écrire
un nouveau symbole,
les têtes de lecture peuvent se déplacer d'une case vers la droite ou vers la gauche à chaque étape.
Les mouvements des têtes de lecture sont guidés par un ensemble de régles fini
fixé à l'avance.
La machine possède aussi un registre d'état permettant de se souvenir de l'état
courant, on définit avant exécution un état initial placé dans ce registre.
Une règle associe à un état et au symbole lu, un symbole à écrire, le prochain état et
le déplacement à effectuer.
Si la machine rencontre un couple état, symbole lu ne possédant pas de règle,
la machine s'arrête alors. Attention cepedant, la machine peut ne jamais s'arrêter.
Les rubans contiennent avant le début de la procédure un nombre fini de symbole
écris dans leurs cases, à l'exception d'un symbole particulier dit blanc, qui est
inscrit une infinité de fois.
On parle de mot pour décrire les symboles non blanc contenu par un ruban avant exécution
de la machine.
On peut définir un certain nombre d'états dit acceptant, si pour un mot donné
la machine s'arrête dans un état acceptant on dit que le mot est accepté par la
machine.

\subsection{Historique}

En 1928, le mathématicien David Hilbert énonce le problème de la décision: existe
t-il une méthode \og{}effectivement calculable\fg{} permettant de décider si une proposition est démontrable ou non
ou non pour un ensemble d'axiomes donné.
Une grande partie du problème réside dans le fait de définir ce qu'est une
méthode effectivement calculable.
C'est ce que va faire Alan Turing en 1936, en introduisant le concept de machine de Turing, dans son article:
On Computable Numbers, With An Application To The Entscheidungsproblem.
Il répond également à la question en démontrant qu'une machine de Turing ne peut
déterminer systématiquement si pour un mot donné une machine de Turing s'arrête ou non.

Par ailleurs, Alan Turing s'est illustré par la cryptanalyse du chiffrement Allemand (machine
Enigma, et machine de Lorenz), en particulier par la création de Colossus, machine
très proche d'un ordinateur mais n'étant paradoxalement pas Turing-complet.

\subsection{Définition formelle et vocabulaire}

\theoremstyle{definition}
\newtheorem{definition}{Définition}[section]

\begin{definition}
 Une machine de Turing à $k$-rubans est la donnée d'un 7-uplet:
 \[M = (Q, \Sigma, \Gamma, B, \delta, q_0, Q_a)\]
où:
\begin{itemize}
  \item $Q$ est l'ensemble fini des états;
  \item $\Sigma$ un alphabet fini;
  \item $\Gamma$, l'alphabet de travail, contenant à $B$ le symbole blanc, et
  incluant $\Sigma$;
  \item $q_0$ l'état initial;
  \item $Q_a$ l'ensemble des états acceptants;
  \item $\delta$ la fonction de transition, avec $\delta: Q \times \Gamma^k \rightarrow Q \times \Gamma^k \times \{ \leftarrow, \rightarrow, - \}^k$ \\
  $\rightarrow$ voulant dire déplacement de la tête de lecture vers la droite,
  $\leftarrow$ vers la gauche et $-$, pas de déplacement.
\end{itemize}

On dira qu'une machine de Turing est semi-fini si on a 8-uplet ajoutant un
symbole $\$$ à l'alphaber $\Gamma$ empêchant tout écriture au dela de ce symbole.
\end{definition}

\begin{definition}
Une \textbf{configuration} est donnée par la description du ruban, par la position de la tête de lecture/écriture, et par l’état interne.
\end{definition}

\begin{definition}
Une machine de Turing \textbf{non-déterministe} est une machine de Turing pour laquelle
la fonction $\delta$ est remplacée par un $\Delta$ un sous-ensemble de
$(Q \times \Gamma^k) \times (Q \times \Gamma^k \times \{ \leftarrow, \rightarrow, - \}^k)$

Remarque: Tout langage accepté par une machine de Turing non déterministe est
accepté par une machine de Turing déterministe.
\end{definition}
\subsection{Utilisation des machines de Turing de nos jours}

Les machines de Turing n'ont pas d'intérêt pratique direct, même avec un ruban
fini, ce n'est pas une architecture physique efficace. L'intérêt théorique
reste cependant fondamental, la machine de Turing donne un modèle théorique
simple à la notion de machine de calcul. Il existe d'autre modèle comme
le $\lambda$-calcul utilisé en parallèle mais, ils n'ont pas remplacé le modèle de Turing.
L'intérêt des machines de Turing est définir la notion de Turing-complet, la
notion de complexité et de permettre de démontrer la décidabilité ou l'indécidabilité
d'un problème.

Un langage informatique est dit Turing-complet s'il permet de représenter toutes
les fonctions calculables au sens de Turing, cela permet par exemple de donner
une définition de langage de programmation.

Une bonne appréhension de ce qui est calculable et décidable de ce qui ne l'est pas permet
de voir les limites des problèmes que peuvent résoudre les ordinateurs. Par
exemple le problème de l'arrêt, limite nos espoirs pour le débogage automatisé.

La machine de Turing permet aussi de définir la théorie de la complexité, si
on a une machine de Turing T, le nombre maximum de déplacement avant arrêt pour les
entrées de taille n définie la complexité en temps. On peut de même définir
la complexité en taille avec le nombre de case du ruban utilisé. Cela permet aussi
de définir le problème \og{}P = NP\fg{}, les classes de complexité
P et NP peuvent être définie par:
\[\mathsf{P} = \bigcup_{k\in\mathbb{N}} \mathsf{TIME}(n^k) \qquad \text{et} \qquad
\mathsf{NP} = \bigcup_{k\in\mathbb{N}} \mathsf{NTIME}(n^k)\]
avec $\mathsf{TIME}(n^k)$ (resp. $\mathsf{NTIME}(n^k)$)
la classe de problèmes qui peuvent être décidé par par une machine de Turing déterministe.

\subsection{But du projet}

Le but du projet est un simulateur de machine de Turing, ce simulateur
accepte des machines de Turing a un ou plusieurs rubans infinis ou semi-finis.
L'idée est de faciliter la compréhension du fonctionnement d'une machine de Turing.
Le simulateur permet à l'aide d'une interface graphique ou d'un fichier de
définir une machine de Turing et ses données initiales, puis de visualiser dans
cette interface les étapes successives engendrées par la machine ainsi que de
revenir en arrière dans son exécution.
Il est également possible de sauvegarder la définition d'une machine de Turing dans un fichier ainsi que l'historique du déroulé d'une simulation.

\subsection{Clients et utilisateurs}

Notre application est à des fins pédagogiques, nos utilisateurs seront
principalement des étudiants découvrant le concept de machine de Turing et
cherchant à comprendre leur fonctionnement. L'application peut aussi être
utilisée dans le contexte d'une vulgarisation scientifique sur le sujet des
machines de Turing.

Notre commanditaire est un universitaire souhaitant mettre à disposition de
ses étudiants ou d'un public intéressé par la vulgarisation scientifique une
illustration du concept de machine de Turing.

\section{Contraintes du projet}

L'application permet de définir et visualiser à l'aide d'une interface graphique une machine déterministe à plusieurs rubans (potentiellement semi-fini) et accepte les wildcards.
Il est possible de sauvegarder dans un fichier la définition d'une machine, ainsi que l'historique d'une simulation.

Les machines Turing pouvant ne pas s'arrêter, un nombre maximal d'étapes
sera fixé, ce nombre maximal n'a pas à être extrêmement élevé vu que le but est la visualisation par des humains et non des simulations complexes.

L'utilisateur doit définir un état initial et peut définir des états finaux acceptant, l'interface affichera si la machine s'arrête sur un tel état à
la fin de la simulation.

La visualisation de la simulation peut se faire à différents rythmes et
il est possible de revenir en arrière, de réinitialiser ou d'aller
directement à la dernière étape.

L'application aide l'utilisateur à détecter d'éventuelles erreurs,
en déterminant si un symbole dans des données ne se trouve pas
l'alphabet ou si ses fonctions de transitions rendent la machine non
déterministe.

\section{Exigences fonctionnelles}

\subsection{Organigramme}

\rotatebox{90}{
    \includegraphics[scale=0.89]{assets/Org20-1.pdf}
    }

\subsubsection{Listes des fonctionnalités de leur coût par module}

\subsubsection*{Interface graphique}

\begin{tabular}{|p{8cm}|l|l|l|}
\hline
Fonctionnalité & Responsable & \multicolumn{1}{c|}{Coût en ligne} & \multicolumn{1}{c|}{Coût en heure} \\ \hline
Afficher aide & Martin & \multirow{19}{*}{650 lignes} & \multirow{19}{*}{ 35 heures} \\ \cline{1-2}
Sélectionner un fichier & Martin & & \\ \cline{1-2}
Saisir nombre de ruban et type & Martin & & \\ \cline{1-2}
Saisir une règle & Martin & & \\ \cline{1-2}
Demander une suppression de règle & Martin & & \\ \cline{1-2}
Saisir états finaux & Martin & & \\ \cline{1-2}
Saisir état initiales & Martin & & \\ \cline{1-2}
Saisir les données initiales sur le rubans & Martin & & \\ \cline{1-2}
Proposer une sauvegarde & Martin & & \\ \cline{1-2}
Afficher rubans et têtes & Wissem & & \\ \cline{1-2}
Afficher règles & Wissem & & \\ \cline{1-2}
Afficher alphabet & Wissem & & \\ \cline{1-2}
Afficher erreur & Wissem & & \\ \cline{1-2}
Mise en évidence de la régle utilisée & Wissem & & \\ \cline{1-2}
Proposer d'avancer à différents rythmes & Wissem & & \\ \cline{1-2}
Proposer de mettre en pause & Wissem & & \\ \cline{1-2}
Proposer retour en arrière & Wissem & & \\ \cline{1-2}
Proposer réinitialisation & Wissem & & \\ \cline{1-2}
Aller à l'état final & Wissem & & \\ \cline{1-2}
Afficher résultat & Wissem & & \\ \hline
\end{tabular}


\subsubsection*{Gestion entrée/Vérificateur}

\begin{tabular}{|p{8cm}|l|l|l|}
\hline
Fonctionnalité & Responsable & \multicolumn{1}{c|}{Coût en ligne} & \multicolumn{1}{c|}{Coût en heure} \\ \hline
Vérifier nombre et type de ruban  & Helmi & \multirow{6}{*}{200 lignes} & \multirow{6}{*}{ 20 heures} \\ \cline{1-2}
Vérifier format règle & Ahmed & & \\ \cline{1-2}
Vérifier présence état initial & Ahmed & & \\ \cline{1-2}
Vérifier état initial & Ahmed & & \\ \cline{1-2}
Vérifier symbole & Ahmed & & \\ \cline{1-2}
Déterminer si la machine est prête & Helmi & & \\ \cline{1-2}
Vérifier déterminisme & Helmi & & \\ \hline

\end{tabular}

\subsubsection*{Simulateur}

\begin{tabular}{|p{8cm}|l|l|l|}
\hline
Fonctionnalité & Responsable & \multicolumn{1}{c|}{Coût en ligne} & \multicolumn{1}{c|}{Coût en heure} \\ \hline
Appliquer instruction & Tahar & \multirow{8}{*}{400 lignes} & \multirow{8}{*}{ 30 heures} \\ \cline{1-2}
Selection instruction à appliquer & Louis & & \\ \cline{1-2}
Generer instruction inverse & Farah & & \\ \cline{1-2}
Stocker étape & Tahar & & \\ \cline{1-2}
Reculer d'une étape & Tahar & & \\ \cline{1-2}
Aller à l'étape initiale & Farah & & \\ \cline{1-2}
Aller à l'étape finale & Farah & & \\ \cline{1-2}
Stocker régles & Louis & & \\ \cline{1-2}
Extraire alphabet & Louis & & \\ \cline{1-2}
Déterminer si l'état est final & Louis & & \\ \hline
\end{tabular}

\subsubsection*{Gestion Fichier}

\begin{tabular}{|p{8cm}|l|l|l|}
\hline
Fonctionnalité & Responsable & \multicolumn{1}{c|}{Coût en ligne} & \multicolumn{1}{c|}{Coût en heure} \\ \hline
Lire les règles d'une machine dans un fichier & Mohamed & \multirow{4}{*}{100 lignes} & \multirow{4}{*}{ 10 heures} \\ \cline{1-2}
Ecrire les règles d'une machine dans un fichier & Mohamed & & \\ \cline{1-2}
Sauvegarder l'historique final d'exécution d'une machine. & Mohamed & & \\ \cline{1-2}
Charger aide & Mohamed & & \\ \hline
\end{tabular}

\subsubsection{Algorithmes}

\begin{algorithm}[H]
\SetAlgoLined
    \textbf{Entrées} :\\
    \textit{Table d'instructions}  \textbf{T};\\
    \textit{Rubans}  \textbf{R1,...,Rn};  \\
    \textit{Positions tetes}  \textbf{p1,...,pn};\\
    \textit{Etat actuel de la machine}  \textbf{E};\\
    \textbf{Sortie} : \\
    \textit{Instruction} \textbf{I}; \\
    \textbf{Debut} : \\

    pre\_compteur, compteur = 0, 0\;
 \textbf{Iterer} sur les instructions de T \\
 \quad \textbf{Si} Etat de l'instrucion correspond a E\\
  {
   \quad\quad \textbf{Iterer} sur les symboles de lecture de l'instruction\\
   {
   \quad\quad\quad{ \textbf{Si} le symbole a lire \textbf{$S_i$} correspond au symbole sur le ruban \textbf{$R_i$} a la position \textbf{$p_i$}} \\
    \quad \quad \quad \quad\quad Incrementer compteur\\
    \quad \textbf{Sinon si} le symbole a lire n'est pas "*" ( Une Wildcard ) \\
    \quad \textbf{ET} le symbole sur le ruban ne coincide pas \\
     \quad \quad Sortie de la boucle car l'instruction ne correspond pas
   }

   }
   \quad \textbf{Si} tout les symboles correspondent  \\
   \quad \textbf{ET} que cette instruction est plus specifique qu'une instruction retenu \\ \quad precedemment ( pre\_comp $<$ compteur ) \\
   \quad\quad Retenir l'instruction dans I \\
   \quad\quad MAJ de pre\_comp a compteur\\

 \textbf{Retour} de I\\
 \textbf{Fin.} \\
 \textbf{Complexite :} O(Nombre d'instructions x nombre de rubans)
 \caption{Algorithme selection instruction a appliquer}
\end{algorithm}

\begin{algorithm}[H]
\SetAlgoLined
    \textbf{Entrée} :\\
    \textit{Instruction} \textbf{I}; \\
    \textbf{Sortie} :\\
    \textit{Instruction Inverse} \textbf{Inv};\\
    \textbf{Debut} : \\

    L'etat final de Inv est initialiser a l'etat initial de I. \\
    Quant a l'etat initial de Inv, il prend la valeur de l'etat final de I.\\

 \textbf{Iterer} jusqu'à n ( Le nombre de symboles et de mouvements )

 {
    \quad \textbf{Initialiser} le symbole a ecrire dans Inv au symbole lu dans I \\
    \quad \textit{// On peut aussi initialiser le symbole de lecture, mais cela n'est pas important car il n'y aura pas de verification lors du retour en arriere.} \\
    \quad \textbf{Si} le mouvement est vers la droite dans I, il sera vers la gauche dans Inv.\\
    \quad \textbf{Sinon si} le mouvement est vers la gauche dans I, il sera vers la droite dans Inv.\\
    \quad \textbf{Sinon} initilisation du mouvement a Stay.\\

 }

 \textbf{Fin.} \\
 \textbf{Complexite :} O(Nombre de rubans)
 \caption{Algorithme generer instruction inverse}
\end{algorithm}

\begin{algorithm}[H]
\SetAlgoLined
    \textbf{Entrées} :\\
    \textit{Rubans}  \textbf{R1,...,Rn};  \\
    \textit{Positions tetes}  \textbf{p1,...,pn};\\
    \textit{Etat actuel de la machine}  \textbf{E};\\
    \textit{Instruction} \textbf{I}; \\
    \textbf{Debut} : \\
    MAJ de l'etat de la machine a l'etat de l'instruction\\
 \textbf{Iterer} jusqu'à n \\

 {
 \quad \textbf{Si} le symbole a ecrire n'est pas "*" ( Wildcard )\\
\quad \quad  {Ecrire le symbole $S_i$ sur le ruban $R_i$ a la position $p_i$ }\\
 {
 \quad \textbf{Si} le mouvement est vers la \textit{Droite} \\
 \quad \quad \textbf{Si} on est au bout du ruban ( tout a droite ) \\
 \quad \quad \quad Inserer un symbole " " ( Blanc ) a la fin \\
 \quad \textbf{Incrementer} la position de la tete pour simuler le mouvement vers la droite \\

 \quad \textbf{Si} le mouvement est vers la \textit{Gauche} \\
 \quad \quad \textbf{Si} on est au debut du ruban ( tout a gauche ) \\
 \quad \quad \quad Inserer un symbole " " ( Blanc ) au debut  \\
 \quad \quad \textbf{Sinon}, \textbf{décrémenter} la position de la tete pour simuler le mouvement vers  la gauche \\

   }
 }

 \textbf{Fin.} \\
 \textbf{Complexite :} O(Nombre de rubans)
 \caption{Algorithme appliquer instruction a la machine}
\end{algorithm}

\begin{algorithm}[H]
\SetAlgoLined
    \textbf{Entrée} :\\
    \textit{Instructions} \textbf{I1,...,Im}; \\
    \textbf{Sortie} :\\
    \textit{Determinisme} \textbf{det};\\
    \textbf{Debut} : \\

m1 = 0 \\
m2 = 0 \\
det = \textit{vrai}\\

\textbf{Tant que} m1 $\leq$ m ( nomrbres d'instructions)\\
\textbf{ET} det = vrai \\

 {
   \quad m2 = m1 + 1\\
    \quad\textbf{Tant que} m2 $\leq$ m \\
    \quad\textbf{ET} det = Vrai \\
    \quad \quad \textbf{Si} etat initial de Im1 = etat initial de Im2\\
    \quad \quad\quad det = \textit{faux}\\
    \quad \quad \quad \textbf{Iterer} jusqu'a n ( Le nombre de rubans ) \\
    \quad \quad \quad \quad \textbf{Si} symbole i de Im1 $\neq$ symbole i de Im2\\
   \quad \quad \quad \quad\quad det = vrai \\
   \quad \quad\quad  \quad\quad \textbf{Sortie} de la boucle\\
   \quad \quad \textbf{Incrementer }m2 \\

   \quad\textbf{Incrementer }m1 \\



 }
\textbf{Retour} det\\
 \textbf{Fin.} \\
 \textbf{Complexite :} O(Nombre d'instructions$^{2}$ x Nombre de Rubans)
 \caption{Algorithme verifier determinisme}
\end{algorithm}


\section{Exigences non-fonctionnelles}

\subsection{Apparence}

L'application doit être simple à prendre en main, une zone contient
les règles saisies par l'utilisateur et une zone pour le ruban.
La règle utilisée à chaque étape est surlignée.
Une zone permet d'avancer dans le déroulé de la simulation à différent
rythme ou d'avancer.

Voir en Annexe.

\subsection{Performance}

Il n'y a d'enjeux critique de performance pour notre projet, l'objectif
est que l'utilisateur ne ressente pas de temps d'attente lors d'une utilisation classique de l'interface graphique.

\section{Autres aspects du projet}

\subsection{Choix du langage}

\begin{itemize}
    \item[\textbf{Performance et sécurité}] Notre application ne posséde pas de contrainte au niveau du temps d'exécution garanti, ou d'exécution sécurisée. Les algorithmes mis en jeu sont mieux que quadratique, et la
    taille des données à traiter est très raisonnable, il n'y a donc pas
    de difficultés majeurs liées aux performances.

    Il n'y a donc pas de raisons de privilégier un langage \og{}compilé\fg{} à
    un langage \og{}interprété\fg{}. De même, le choix d'un langage proposant une gestion de mémoire par \og{}ramasse-miette\fg{} ne pose pas de problème.
    \item[\textbf{Système}] Il n'y a pas de programmation \og{}système\fg{} dans notre projet.

    Vu les éléments précédents, nous pouvons éliminer des langages
    tels que C, C++ ou Rust.
    \item[\textbf{Maintenabilité}] Le projet étant de taille raisonnable, choisir un langage proposant un typage dynamique ne pose de problème, il
    est néanmoins préférable qu'il supporte les indications/annotations de
    type.
    \item[\textbf{Paradigme}] Les modules, Vérification et Gestion de
    fichier la programmation procédurale ou fonctionnelle semblent adaptés.
    Le module d'Interface graphique se prête relativement bien à la modélisation orientée objet. \\
    Dans le module Simulateur, nous avons identifié des éléments qui se prêtent
    à la modélisation objet, une machine (qui contient les fonctions de transitions et les états particuliers), des rubans (symbole et tête de lecture) et un simulateur composé d'un état, une machine et de rubans.
    Les notions d'objets, de méthode, d'encapsulation et d'envoie de message (en lien aussi
    avec l'interface graphique) nous semble utile, dans ce contexte, même
    si nous ne pensons pas utiliser l'hérédité, la surcharge ou le polymorphisme, il semble judicieux de choisir un langage permettant d'utiliser facilement la programmation orientée objet. \\
    C'est pour cela que nous avons fait le choix d'un langage multiparadigme.
\end{itemize}

\medskip

Une vaste gamme de langage de programmation semble envisageable, de Python
à CLisp, en passant par Go, Ocaml, Ruby, Lua, Kotlin, Scala.
Parmi ces langages celui semblant être le plus utilisé est Python, nous avons
donc fait ce choix.

\section*{Annexes}

\begin{minipage}{0.5\linewidth}
    \includegraphics[scale=0.3]{assets/interface_saisie.png}
\end{minipage}
\hfill
\begin{minipage}{0.5\linewidth}
    \includegraphics[scale=0.3]{assets/interface_simulation.png}
\end{minipage}




\end{document}
