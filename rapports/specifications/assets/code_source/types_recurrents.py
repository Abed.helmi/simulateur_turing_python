from typing import NewType, Tuple, Set, Dict
from enum import Enum

Etat = NewType("Etat", str)

# Un symbole est une chaîne de longueur 1
Symbole = NewType("Symbole", str)
# Symboles contient nbr_rubans x Symbole
Symboles = Tuple[Symbole, ...]

# Les Mouvement sont défini par une énumération
class Mouvement(Enum):
    GAUCHE = -1
    SUR_PLACE = 0
    DROITE = 1

# Mouvements contient nbr_rubans x Symbole
Mouvements = Tuple[Mouvement, ...]

# Clef correspond à l'entrée de la fonction de transition
Clef = Tuple[Etat, Symboles]
# Une valeur correspond à la sortie de la fonction de transition
Valeur = Tuple[Etat, Symboles, Mouvements]
# Une transition associe à un état et aux symboles lus
# un nouvel état, des symboles à écrire et les prochains mouvements
Transition = Tuple[Clef, Valeur]
# Une table de transition est un tableau associatif
# associant à une clef une valeur
Table_transition = Dict[Clef,Valeur]

# Un Alphabet est l'ensemble des symboles que l'on trouve dans
# dans la table de transition.
# Si le ruban est semi-infini, il contient le symbole $
Alphabet = Set[Symbole]