class Machine():
    def __init__(self):
        self.etat_init: Etat = ""
        self.etat_finaux: Set[Etat] = []
        self.table_transition: Table_transition = dict()
        
    def remplir_dictionnaire(liste_règle: List[str]) -> None
        """
        Remplie le dictionnaire avec la liste de règles provenant de 
        l'interface graphique ou d'un fichier en paramètre.
        Utilise la fonction 'ajouter_transition'. 
        Remplie en même temps les états finaux et l'état initial.
        """

    def __ajouter_transition__(self, regle_saisie: Transition) -> None:
        """
        Ajoute la 'regle_saisie' dans le dictionnaire.
        Est appelé dans la fonction 'remplir_dictionnaire'
        """

    def __selection_valeur__(self, clef: Clef) -> Valeur:
        """
        Cette fonction sélectionne la valeurs a appliquer dans 
        la table de transitions. 
        Si la clé ne correspond a aucun élément, None est envoyé 
        et l'exception doit être gérer. (arrêt de la machine)
        Exception:
            FinExecutionMachine
        """ 
    
    def appliquer_valeur(self, etat_actuel: Etat, valeur: Valeur, listeRubans: List[Ruban]) -> None:
        """ 
        A partir de la valeur extraite de la fonction 
        précédente, on procède au déroulement de la machine, 
        c'est a dire : écrire les symboles sur les rubans,
        bouger les têtes de lectures et changer l'état courant 
        de la machine
        """
    
    def inverser_regle(self, regle: Transition) -> Transition:
        """
        retourne la transition à appliquer pour revenir à l'étape 
        précédente de l'exécution de la machine. 
        """
    
    def appliquer_regle_inverse(self, listeRubans: List[Ruban], transition: Transition) -> None:
        """
        Appliquer une règle inversée a la machine. 
        Le traitement est différent entre les règles normales et 
        les règles inversées, car dans le cas des règles inversées, 
        le mouvement des têtes doit s'effectuer avant l'écriture. 
        Et il faut aussi procéder a la suppression des 
        éléments précédemment écrit au bout des rubans.
        """
    
    def extraire_alphabet(self) -> Alphabet:
        """
        Renvoie l'ensemble des symboles contenus dans 
        les règles de transition.
        """
        pass
        
    def est_sur_etat_final(self, etat_courant: Etat) -> bool:
        """
        Déterminer si l'état courant est final. Appelé
        lors de la fin de l'exécution.
        """
        pass
