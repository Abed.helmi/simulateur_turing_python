class Ruban():
    def __init__(self, symboles: List[Symbole], t_ruban) -> None:
        """
        Le premier paramètre est une liste de symboles représentant 
        les symboles écrit sur le ruban. 
        Le deuxième est la position de la tète sur le ruban.
        Le troisième représente le type du ruban, le choix se 
        fera entre ruban semi-fini ou ruban infini. 
        """
        self.symboles: List[Symbole] = ...
        self.type_ruban: {SEMIFINI, INFINI} = ...
        self.position: int = ...
        self.etat_initial: Symboles = ...

    # get et set basiques


    def view(self, taille_vue: int) -> List[Symbole]:
        """
        A destination de l'interface graphique, seule une partie 
        du ruban est affichée.
        Cette méthode retourne une sous liste de la liste 
        représentant les symboles sur le ruban. 
        Cette sous liste comprend les symboles dans 
        l'intervalle centré sur position et de longueur 2x taille vue.
        La méthode devra s'adapter et inclure des symboles blancs
        si la vue est plus grande que le ruban, pour remplir 
        les vides dans la sous liste.
        """
        
    def recupere_symbole_lu(self) -> Symbole:
        """
        Renvoie le symbole correspondant à la position de 
        la tête de lecture.
        """
    
    def appliquer_changement(self, mouvement: Mouvement, s: Symbole): -> None:
        """
        Reçoit le mouvement et le symbole correspondant à la 
        transition appliquée pour ce ruban.
        Utilise les méthodes privés, modifier_position et 
        ecriture_symbole.
        """

    def __modifier_position__(self, mouvement: Mouvement) -> None:
        """
        Cette méthode modifie la position de la tête de lecture
        en fonction du mouvement, droite, gauche ou sur place donnée
        """
        
   def __ecriture_symbole__(self, s: Symbole) -> None:
        """
        Cette méthode modifie le symbole qui se trouve à la position
        de la tête de lecture et le remplace par le symbole s passé
        en paramètre.
        Si symbole est une wildcard, le symbole n'est pas remplacé.
        """