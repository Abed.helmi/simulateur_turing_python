def lecture_fichier_machine(chemin: str) -> None:
    """
    Création d'une machine depuis la lecture d'un fichier dont 
    l'emplacement est précisé en paramètre.
    On commence par lire le nombre de ruban qui est vérifié 
    par la fonction vérificateur.verif_nbr_rubans. 
    Une machine est crée, cette fonction lui donne état initial 
    et états finaux en passant par vérificateur.verif_etat.
    Puis les règles sont vérifiés une à une par la fonction 
    vérificateur.verif_format_regle. Chaque règle amène 
    une appel de la fonction dérouleur.ajouter_transition pour 
    les ajouter à la machine.
    """


def ecriture_fichier_machine(nbr_ruban: int, machine: Machine, chemin: str) -> None:
    """
    Il reste à préciser en fonction de l'interface graphique, si le 
    chemin est une chaîne de caractère ou un type spécifique aux
    chemins de fichier.
    Respecte le format précisé pour la sauvegarde de machine, 
    récupère état initial, états finaux depuis la machine, ainsi 
    que toutes les règles de transitions.
    
    Écriture fichier dans fichier .txt, dont le nom a été choisi
    par l'utilisateur dans l'interface graphique.
    """
    

    
def sauvegarder_historique_execution(rubans: List[Ruban], pile_regles: List[Transition]) -> None:
    """
    L'état initial des rubans est récupéré et écrit ligne par ligne
    chaque symbole est séparé par un espace.
    En suite, la règle utilisée à chaque étape de l'exécution 
    est récupéré depuis liste_regles et écrite ligne par ligne 
    (en respectant le format d'écriture des règles).
    
    Fonction déclenchée lorsqu'on arrive à une fin d'exécution. 
    Écriture dans fichier nommé timestamp.txt.
    """
    
def charger_aide() -> None:
    """ 
    Ouvre une page html d'aide dans le navigateur de l'utilisateur
    """
    


def traduire_regle(regle: Transition) ->str
    """
    Cette fonction va traduire une règle en chaine de caractère, 
    afin de l'inscrire dans le fichier de sauvegarde.
    
    Cette fonction va être appelé dans la fonction 
    écrire_fichier_machine().
    """