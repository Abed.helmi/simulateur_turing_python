from types_recurrents import *


def verif_nbr_rubans(nbr_rubans_saisi: str) -> int:
    """
    Détermine si le nombre de rubans saisi est un nombre strictement
    positif.
    Le type du paramètre peut varier selon l'interface graphique.
    Appelé lorsque l'utilisateur entre le nombre de ruban,
    Utiliser alors pour fixer la variable globale etat_courant 
    du module dérouleur
    Exceptions:
        IllegalValue
    """
    pass


def presence_etat_init(etat_init: Etat) -> Etat:
    """
    Détermine si un état initiale a été saisi et le
    renvoie.
    Exceptions:
        ValeurNonPrecisee
    """
    pass


def verif_format_regle(regle_saisie: str, nbr_rubans: int) -> Transition:
    """
    L'utilisateur saisie une règle/instruction sous
    forme d'une chaîne de caractère, cette fonction vérifie
    que le format est correct.
    Utilise la fonction verif_fin_ruban.
    Le format précis d'une règle est décrit dans la partie 
    gestion de fichier. 
    Est appelé à l'intérieur de la fonction d'ajout d'un
    élément à la table de transition lorsqu'une règle est saisie.
    """
    pass

def verif_etat(etat_a_verifie : Etat) -> Etat
    """
    Cette fonction vérifie que l'état passé en paramètre correspond à nos contrainte de nom des états.
    Une exception pourra être levé ici.
    
    Cette fonction sera appelé dans lecture_fichier_machine() 
    dans le module de "gestion de fichier". (Et c'est pour le fait qu'elle n'est appeler nul part ailleurs qu'elle ne figure pas dans le cahier des charges.)
    """


def verif_fin_ruban(regle_saisie: str, nbr_rubans: int) -> bool:
    """
    Fonction appelé par verif_format_regle dans le cas des rubans
    semi-finis, et qui détermine si une transition ne réécrit pas
    pas le symbole $, et ne le dépasse pas
    Exceptions:
        IllegalValue
    """
    pass


def verif_symboles_saisis(init_ruban: str, alphabet: Alphabet) -> str:
    """
    Vérifie les symboles initiaux remplis par l'utilisateur
    sur un ruban donné. La vérification consiste à déterminer
    si tout les symboles sont dans l'alphabet, si ce n'est pas
    le cas, la machine de Turing n'a, a priori, pas de sens pour
    ces données initiales.
    Exceptions:
        IllegalValue
    """
    pass


def est_deterministe(table_transi: Table_transition) -> bool:
    """
    Détermine si une table de transition est déterministe, càd
    si elle ne contient pas différentes règles qui à un même
    antécédent associe des images différentes.
    Est appelé dans le module dérouleur.
    """
    pass

