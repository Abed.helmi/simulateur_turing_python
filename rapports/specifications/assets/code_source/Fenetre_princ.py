class FenetrePrincipale(QMainWindow):
    """
    Fenêtre principale de l'application, contient 
    un menu, et la page en train d'être affiché.
    Le menu permet de charger, créer, ou sauvegarder une machine.
    Contient les fenêtres de dialogue gérant ces actions.
    """

    def __init__(self):
        self.cadre_central = PageAccueil(self)
        self.cree_actions()
        self.setupUI()

    def setupUI(self):
        self.setCentralWidget(self.cadre_central)
        self.cree_menus()
    
    def cree_actions(self):
        """
        Création des action du menu dans la barre de menu
        """
        #Action de charger une machine depuis un fichier
        self.ouvrir_fichier_act = QAction('Charger une machine', self)
        self.ouvrir_fichier_act.triggered \
                               .connect(self.dialogue_charger_fichier)
        
        #Action de sauvegarder une machine dans un fichier
        self.sauver_fichier_act = QAction("Sauvegarder la machine", self)
        self.sauver_fichier_act.triggered.connect(self.dialogue_sauver_fichier)
        
        #Action de quitter l'application
        self.quitter_act = QAction('&Quitter', self)
        self.quitter_act.triggered.connect(app.quit)
        
        #Action qui ouvre l'aide
        self.aide_act = QAction('&Aide', self)
        self.aide_act.triggered.connect()

    
    def affiche_aide(self):
        """
        Fonction qui ouvre l'aide.
        Utilise la fonction d'aide de la gestion de fichiers.
        """
    
    def cree_menus(self):
        """
        Création de la barre de menu
        """
        
    def changer_page(self, page: QFrame):
        """
        affiche la page 'page' dans la fenêtre
        """
    
    def dialogue_creer_machine(self):
        """
        Permet de créer la fenêtre de dialogue demandant le 
        nombre et le type de Ruban
        """
       
    def dialogue_sauver_fichier(self):
        """
        Création d'un dialogue pour sauvegarder un fichier en 
        appelant la méthode de la gestion de fichier
        'écriture_fichier_machine'
        Utilise le Widget QFileDialog
        """
    
    def dialogue_charger_fichier(self):
        """
        Création d'un dialogue pour charger un fichier en appelant 
        la méthode de la gestion de fichier 'lecture_fichier_machine'
        
        Utilise le Widget QFileDialog
        
        Appelle la fonction de réinitialisation du module dérouleur.
        Appelle la fonction lecture d'un fichier, et initialise une nouvelle machine.
        """

class DialogueCreation(QDialog):
    """
    Le fenêtre de dialogue pour la sélection du nombre 
    de rubans et leur type. Amène à la page de saisie 
    des règles. 
    Assez complexe pour mériter sa propre classe.
    """
    
    def __init__(self, parent):
        self.setupUI(parent)

    def setupUI(self, parent):
        self.saisie_nbr_ruban = QComboBox(self)
        self.saisie_type_ruban = QCheckBox("Ruban fini à gauche:")
        self.btn_validation = QPushButton("Valider", self)
        self.btn_validation.clicked \
                           .connect(lambda: self.validation_creation(parent))

    def validation_creation(self, parent):
        """
        Appelé lorsque la création est validé par l'utilisateur.
        Transmet le nombre de Ruban au dérouleur.
        Appelle la fonction de réinitialisation du module dérouleur.
        Amène à la création du nombre de Ruban correspondant 
        avec le bon type dans le dérouleur.
        
        Change la page pour passer à la page de Saisie de la 
        machine.
        """
