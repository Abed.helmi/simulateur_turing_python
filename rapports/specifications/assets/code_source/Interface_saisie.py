class Saisie(QFrame):
    
    def __init__(self):
        self.setUI()
    
    def setUI(self):
        #Case à cocher pour 'état initial' et 'état final'
        init = QCheckBox("etat init")
        final = QCheckBox("etat final")
        
        #zone de texte des règles
        texte_regle = QLineEdit()
        
        #zone de texte de l'initialisation des rubans
        texte_init_ruban = QLineEdit()
        
         #zone d'affichage des règles déjà rentrées
        liste = QListWidget()    
        
        #bouton d'ajout à la liste
        bouton = QPushButton("Ajouté", self)     
        bouton.clicked.connect(lambda: self.ajout_Item(texte.text(), liste, init, final))
        
        #bouton de suppression d'une règle dans la Qliste
        bouton_supp = QPushButton("supp", self) 
        bouton_supp.clicked.connect(lambda: texte2.takeItem(liste.currentRow()))

        #bouton qui retourne la liste des règles
        bouton_lancer = QPushButton("Lancer", self)
        bouton_go.clicked.connect(lambda: self.lancer_machine(liste, texte_init_ruban))
        

        def cons_liste(self, list:QListWidget) -> list:
            """
            Construit une liste de règles et la retourne
            """

        
        def ajout_Item(self, item:str, list:QListWidget, init:QCheckBox, final:QCheckBox) -> None:
            """
            Ajoute une règle dans la Qliste qui est affiché et vérifie si la règle n'est pas vide. Ajoute 'I' à 'Item' si 'init' est coché et un 'F' si 'final' est coché.
            """
        def lancer_machine(liste:QListWidget, texte_init_ruban:QLineEdit)
            """
            Transforme 'liste' en une liste python lisible pour le dérouleur en appelant 'cons_liste'. Transforme le 'texte_init_ruban' en un str lisible pour le dérouleur. Donne ensuite ses données au dérouleur en appelant 'remplir_dictionnaire'
            Passe à la page de simulation.
            """
               
               

