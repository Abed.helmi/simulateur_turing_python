class Accueil(QFrame):
    """
    Page d'accueil de l'interface graphique.
    Contient trois boutons, créer une machine, 
    charger une machine, quitter.
    
    Utilise les méthodes de sa fenêtre parent 
    pour permettre d'afficher les dialogues de 
    ces actions.
    """
    def __init__(self, parent):
        self.setupUI(parent)

    def setupUI(self, parent):
        
        #Bouton pour créer une nouvelle machine
        self.btn_nv_machine = QPushButton()
        self.btn_nv_machine.clicked \
                           .connect(parent.dialogue_creer_machine)
                           
        #Bouton pour charger une machine depuis un fichier
        self.btn_charger_machine = QPushButton()
        self.btn_charger_machine.clicked \
                                .connect(parent.dialogue_charger_fichier)
        
        #Bouton pour quitter l'application
        self.btn_quitter = QPushButton()
        self.btn_quitter.clicked.connect(app.quit)
