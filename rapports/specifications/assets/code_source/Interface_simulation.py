class Simulation(QFrame):
    """
    Page permettant de visualiser et d'actionner le déroulement 
    de la simulation.
    Permet d'avancer ou de reculer dans les étapes 
    de la simulation.
    """

    def __init__(self, parent):
        self.setupUI()
        
    def setupUI(self):
        
        # liste de règles
        self.liste_regles = QListWidget()
        
        #liste alphabet
        self.liste_alphabet = QListWidget()
        
        # Zone d'affichage des rubans et de la position
        # de la tête de lecture.
        self.affichage_ruban = AffichageRubans()
        
        # Message de fin d'exécution, affiche si l'état est final
        self.affichage_fin = QLabel()
        
        #Boutons de manipulation de la vitesse de l'éxécution
        self.play = QPushButton(...)
        self.accelere = QPushButton(...)
        self.ralentir = QPushButton(...)
        self.end = QPushButton(...)
        self.debut = QPushButton(...)
        
        
    def remplir_liste(liste_regle: List[str]) -> None:
        """
        Remplir la QListWidget avec les règles de la machine.
        """
    
    def regle_utilise(Regle: Transition) -> None:
        """
        Reçois la règle utilisé afin de l'afficher sur 
        l'interface de simulation.
        """
        
    def play(self) -> None:
        """ Exécute la machine, étape par étape. """
    
    def pause(self) -> None:
        """ Met sur pause l'exécution. """
    
    def avance(self) -> None:
        """ Avance l'exécution de la machine d'une étape. """
    
    def recule(self) -> None:
        """ Reviens en arrière d'une étape sur l'exécution. """
    
    def debut(self) -> None:
        """ Reviens au début de la simulation. """
    
    def fin() -> None:
        """
        exécute la simulation à la vitesse de l'ordinateur pour
        arriver directement à la fin de l'exécution.
        """
        
    def afficheResultat(self) -> None:
    """ 
    Affiche le résultat de l'exécution après 
    qu'elle se soit terminé.
    """
    
    def affiche_alphabet(self) -> None:
    """ Affiche l'aphabet de la machine. """

class AffichageRubans(QFrame):
    """ 
    Zone d'affichage des rubans. 
    A chaque étape d'exécution met à jour son affichage
    en fonction de qui est reçu par la méthode view des rubans.
    """